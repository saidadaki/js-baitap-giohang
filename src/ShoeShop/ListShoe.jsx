import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoe
          handleProductDetail={this.props.handleViewShoeDetail}
          handleAddToCartList={this.props.handleAddToCartList}
          key={index}
          dataShoe={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
