import React, { Component } from "react";
import CartShoe from "./CartShoe";
import DetailShoe from "./DetailShoe";

import ListShoe from "./ListShoe";
import { shoeArr } from "./ShoeData";

export default class ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };

  handleViewDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      cloneCart.push({ ...shoe, soLuong: 1 });
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };

  handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };

  handleDeleteShoe = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <nav className="bg-dark text-center mx-auto text-light py-3 h2 ">
          ONLINE SHOE SHOP
        </nav>
        <ListShoe
          list={this.state.shoeArr}
          handleViewShoeDetail={this.handleViewDetail}
          handleAddToCartList={this.handleAddToCart}
        />
        <CartShoe
          handleDeleteShoe={this.handleDeleteShoe}
          handleChangeQuantity={this.handleChangeQuantity}
          cart={this.state.cart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
