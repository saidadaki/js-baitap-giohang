import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, shortDescription, image } =
      this.props.detailShoe;

    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Detail Shoe
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">X</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="card text-center">
                  <img
                    className="card-img-top mx-auto d-block"
                    style={{ width: 350 }}
                    src={image}
                  />
                  <div className="card-body">
                    <h4 className="card-title">{name}</h4>
                    <p className="card-text">${price}</p>
                  </div>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">{description}</li>
                    <li className="list-group-item">{shortDescription}</li>
                  </ul>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
